import React,{useState} from 'react'
import {Redirect} from 'react-router-dom'
import { connect } from 'react-redux'
import {signUp} from '../../store/actions/authActions'

const SignUp = ({auth, signUp, authError}) => {
    let [email,setEmail] = useState('')
    let [password,setPassword] = useState('')
    let [firstName, setFirstName] = useState('')
    let [lastName, setLastName] = useState('')

    const handleChange = (e) => {
        if(e.target.id === 'email') setEmail(e.target.value)
        else if(e.target.id === 'password') setPassword(e.target.value)
        else if(e.target.id === 'firstname') setFirstName(e.target.value)
        else if(e.target.id === 'lastname') setLastName(e.target.value)
    }
    const onSubmit = (e) => {
        e.preventDefault()
        signUp({
            email,password,firstName,lastName
        })
    }

    if(auth.uid) return <Redirect to='/' />
    return(
        <div className="container">
            <form className="white" onSubmit={onSubmit}>
                <h5 className="grey-text text-darken-3">Sign In</h5>
                <div className="input-field">
                    <label htmlFor="email">Email</label>
                    <input type="email" id="email" value={email} onChange={handleChange} />
                </div>
                <div className="input-field">
                    <label htmlFor="password">Password</label>
                    <input type="password" id="password" value={password} onChange={handleChange} />
                </div>
                <div className="input-field">
                    <label htmlFor="firstname">First Name</label>
                    <input type="text" id="firstname" value={firstName} onChange={handleChange} />
                </div>
                <div className="input-field">
                    <label htmlFor="lastname">First Name</label>
                    <input type="text" id="lastname" value={lastName} onChange={handleChange} />
                </div>
                <div className="input-field">
                    <button className="btn pink lighten-2 z-depth-0">
                        Sign Up
                    </button>
                    {
                        authError && 
                        (<div className="red-text center">
                            <p>{authError}</p>
                        </div>
                        )
                    }
                </div>
            </form>
        </div>
    )
}

const mapStateToProps = (state) => ({
    auth: state.firebase.auth,
    authError: state.auth.authError
})

const mapDispatchToProps = (dispatch) => ({
    signUp : (newUser) => dispatch(signUp(newUser))
})

export default connect(mapStateToProps,mapDispatchToProps)(SignUp)