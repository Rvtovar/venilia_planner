import React,{useState} from 'react'
import {connect} from 'react-redux'
import {signIn} from '../../store/actions/authActions'
import {Redirect} from 'react-router-dom'

const SignIn = ({signIn, authError, auth}) => {
    let [email,setEmail] = useState('')
    let [password,setPassword] = useState('')
    const handleChange = (e) => {
        if(e.target.id === 'email') setEmail(e.target.value)
        else if(e.target.id === 'password') setPassword(e.target.value)
    }
    const onSubmit = (e) => {
        e.preventDefault()
        signIn({email,password})
    }

    if(auth.uid) return <Redirect to='/' />
    return(
        <div className="container">
            <form className="white" onSubmit={onSubmit}>
                <h5 className="grey-text text-darken-3">Sign In</h5>
                <div className="input-field">
                    <label htmlFor="email">Email</label>
                    <input type="email" id="email" value={email} onChange={handleChange} />
                </div>
                <div className="input-field">
                    <label htmlFor="password">Password</label>
                    <input type="password" id="password" value={password} onChange={handleChange} />
                </div>
                <div className="input-field">
                    <button className="btn pink lighten-2 z-depth-0">
                        Login
                    </button>
                    {
                        authError && 
                        (<div className="red-text center">
                            <p>{authError}</p>
                        </div>)
                    }
                </div>

            </form>
        </div>
    )
}

const mapDispatchToProps = (dispatch) => ({
    signIn: (creds) => dispatch(signIn(creds))
})

const mapStateToProps = (state) => ({
    authError: state.auth.authError,
    auth: state.firebase.auth
})

export default connect(mapStateToProps,mapDispatchToProps)(SignIn)