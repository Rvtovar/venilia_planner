import React,{useState} from 'react'
import {createProject} from '../../store/actions/projectActions'
import {connect} from 'react-redux'
import {Redirect} from 'react-router-dom'

const CreateProject = ({createProject, auth,history}) => {
    let [title, setTitle] = useState('')
    let [content, setContent] = useState('')
    const handleChange = (e) => {
        if(e.target.id === 'title') setTitle(e.target.value)
        else if(e.target.id === 'content') setContent(e.target.value)
    }
    const onSubmit = (e) => {
        e.preventDefault()
        createProject({title,content})
        history.push('/')
    }


    if(!auth.uid) return <Redirect to='/signin' />
    return(
        <div className="container">
            <form className="white" onSubmit={onSubmit}>
                <h5 className="grey-text text-darken-3">Create New Project</h5>
                <div className="input-field">
                    <label htmlFor="title">Title</label>
                    <input type="text" id="title" value={title} onChange={handleChange} />
                </div>
                <div className="input-field">
                    <label htmlFor="content">Content</label>
                    <textarea 
                        value={content} 
                        id="content" 
                        className="materialize-textarea"
                        onChange={handleChange}
                    />
                </div>
                <div className="input-field">
                    <button className="btn pink lighten-2 z-depth-0">
                        Create Project
                    </button>
                </div>
            </form>
        </div>
    )
}

const mapStateToProps = (state) => ({
    auth: state.firebase.auth
})

const mapDispatchToProps = (dispatch) => ({
createProject: (project) => dispatch(createProject(project))
})

export default connect(mapStateToProps,mapDispatchToProps)(CreateProject)