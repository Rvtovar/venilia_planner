import React from 'react';
import ReactDOM from 'react-dom';
import 'materialize-css/dist/css/materialize.css'
import 'materialize-css/dist/js/materialize'
import './index.css';
import App from './App';
import {createStore,applyMiddleware, compose} from 'redux'
import rootReducer from './store/reducers/rootReducer'
import * as serviceWorker from './serviceWorker';
import {Provider, useSelector} from 'react-redux'
import thunk from 'redux-thunk'
import firebase from 'firebase/app'
import {ReactReduxFirebaseProvider, isLoaded} from 'react-redux-firebase'
import {createFirestoreInstance} from 'redux-firestore'
import fbConfig from './config/fbConfig'

const store = createStore(rootReducer,compose(applyMiddleware(thunk)))

const rrfConfig = {
  ...fbConfig,
  userProfile: 'users',
  useFirestoreForProfile: true,
  attachAuthIsReady:true,
}

const rrfProps = {
  firebase,
  config: rrfConfig,
  dispatch: store.dispatch,
  createFirestoreInstance,
}

// userProfile: 'users', // where profiles are stored in database
// presence: 'presence', // where list of online users is stored in database
// sessions: 'sessions'

const AuthIsLoaded = ({children}) => {
  const auth = useSelector(
    (state) => state.firebase.auth
  )
  if(!isLoaded(auth)){
    return (
      <div className="">
        'Loading Screen'
      </div>
    )
  }
  return children
}

ReactDOM.render(
    <Provider store={store}>
     <ReactReduxFirebaseProvider {...rrfProps}>
      <AuthIsLoaded>
        <App />
      </AuthIsLoaded>
     </ReactReduxFirebaseProvider>
    </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
